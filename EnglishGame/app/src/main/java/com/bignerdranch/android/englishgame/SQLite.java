package com.bignerdranch.android.englishgame;

import android.content.ContentValues;
import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

/**
 * Created by dmelechow on 11.08.2018.
 */
public class SQLite extends SQLiteOpenHelper {

    private final static String DB_NAME = "english";
    private final static int DB_VERSION = 1;

    public SQLite(Context context) {
        super(context, DB_NAME, null, DB_VERSION);
    }


    @Override
    public void onCreate(SQLiteDatabase sqLiteDatabase) {
        sqLiteDatabase.execSQL("CREATE TABLE DRINK ("
                + "id INTEGER PRIMARY KEY AUTOINCREMENT,"
                + "NAME TEXT,"
                + "DESCRIPTION TEXT,"
                + "RUSSIA TEXT);");
        insertDrink(sqLiteDatabase,"Gutter","Гай","Парень");









    }


    private void insertDrink(SQLiteDatabase sqLiteDatabase, String name, String description, String resourcedId) {
        ContentValues drinkValuew = new ContentValues();
        drinkValuew.put("NAME",name);
        drinkValuew.put("DESCRIPTION",description);
        drinkValuew.put("RUSSIA",resourcedId);
        sqLiteDatabase.insert("DRINK",null,drinkValuew);

    }

    @Override
    public void onUpgrade(SQLiteDatabase sqLiteDatabase, int oldVersion, int newVersion) {
        if(oldVersion==1){

        }
       if (oldVersion==3){

       }
    }

}