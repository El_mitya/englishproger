package com.bignerdranch.android.englishgame;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.view.ViewCompat;
import android.support.v4.view.ViewPropertyAnimatorListener;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.view.animation.CycleInterpolator;
import android.widget.ImageView;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {

    private ImageView buttonStart,buttonStar;

   private  Animation animTranslate;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        buttonStart = (ImageView) findViewById(R.id.imageStart);
        buttonStar = (ImageView) findViewById(R.id.imageStar);
        buttonStar.setOnClickListener(this);
        buttonStart.setOnClickListener(this);
        animTranslate = AnimationUtils.loadAnimation(MainActivity.this, R.anim.anim_translate);

    }

    public void onClick(final View v) {
        ViewCompat.animate(v)
                .setDuration(200)
                .scaleX(0.9f)
                .scaleY(0.9f)
                .setInterpolator(new CycleInterpolator(1))
                .setListener(new ViewPropertyAnimatorListener() {
                    @Override
                    public void onAnimationStart(final View view) {

                    }

                    @Override
                    public void onAnimationEnd(View view) {
                        switch (v.getId()) {
                            case R.id.imageStart:

                               buttonStar.startAnimation(animTranslate);

                               break;
                            case R.id.imageStar:
                                Intent intent = new Intent(MainActivity.this,AppendixOne.class);
                                startActivity(intent);
                        }
                    }
                    @Override
                    public void onAnimationCancel(final View view) {

                    }
                })
                .withLayer()
                .start();
    }
}
