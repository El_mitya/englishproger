package com.bignerdranch.android.englishgame;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.widget.ProgressBar;

/**
 * Created by dmelechow on 11.08.2018.
 */
public class NullActivity extends AppCompatActivity {
   private ProgressBar progressBar;
    private int progress = 0;


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.null_activity);


        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                startActivity(new Intent(
                        NullActivity.this, MainActivity.class));
                finish();
            }
        }, 2000);
    }
}