package com.bignerdranch.android.englishgame;

/**
 * Created by dmelechow on 30.07.2018.
 */
public class HandlingResponses  {
    private int text_id;

    private String transcription;

    private String rightAnswer;

    public HandlingResponses(int text_id, String transcription, String rightAnswer) {

        this.text_id = text_id;

        this.transcription = transcription;

        this.rightAnswer = rightAnswer;
    }

    public int getText_id() {

        return text_id;
    }

    public void setText_id(int text_id) {

        this.text_id = text_id;
    }

    public String getTranscription() {
        return transcription;
    }

    public void setTranscription(String transcription) {
        this.transcription = transcription;
    }

    public String getRightAnswer() {
        return rightAnswer;
    }

    public void setRightAnswer(String rightAnswer) {
        this.rightAnswer = rightAnswer;
    }
}
