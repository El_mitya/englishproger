package com.bignerdranch.android.englishgame;

import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.ViewCompat;
import android.support.v4.view.ViewPropertyAnimatorListener;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.CursorAdapter;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import java.util.Arrays;
import java.util.List;
import java.util.Random;

/**
 * Created by dmelechow on 30.07.2018.
 */
public class AppendixOne extends AppCompatActivity implements View.OnClickListener {
    List<String> answers;

    private SQLiteDatabase db;
    private Cursor mCursor;

   private ImageView mImageView1,mImageView2,mImageView3,mImageView4;


    int position;
    int rightAnswerRandomPosition;
    private Intent intent;
    private RelativeLayout mRelativeLayout;
    CursorAdapter listAdapter;
    private TextView textTrascription;

    private Button variant1;

    private Button variant2;

    private Button variant3;

    private Button variant4;

    private TextView textWorldView;
 private Animation animTranslate,animAlpha;

    private HandlingResponses[] mQurstinBank = new HandlingResponses[]{


            new HandlingResponses(R.string.Hause_gutter, "  гай", "Парень"),
            new HandlingResponses(R.string.Hause_chimney, "  guy", "Парень"),
            //asdasdsa
            new HandlingResponses(R.string.Hause_dormerWindow, "  guy", "Парень"),
            new HandlingResponses(R.string.Hause_eaves, "  guy", "Парень"),
            new HandlingResponses(R.string.Hause_froonDoor, "  guy", "Парень"),


            new HandlingResponses(R.string.Hause1_gutter, " гай", "Парень"),

            new HandlingResponses(R.string.Hause2_the, " ??:", "определенный артикль"),

            new HandlingResponses(R.string.Hause3_and, " ?nd ", "и; а, но"),



            new HandlingResponses(R.string.Hause4_to, " tu:", "к, в, на, до, для"),



            new HandlingResponses(R.string.Hause5_was, " ", "был, была, было"),



            new HandlingResponses(R.string.Hause6_I," ?i", "я"),



            new HandlingResponses(R.string.Hause7_of," ?v", "из, от, о, об"),



            new HandlingResponses(R.string.Hause8_that," ??t", "тот, та, то"),



            new HandlingResponses(R.string.Hause9_you," ju:", "ты, вы"),



            new HandlingResponses(R.string.Hause10_he," hi:", "он"),



            new HandlingResponses(R.string.Hause11_it," it", "она, оно; это"),



            new HandlingResponses(R.string.Hause12_in," in", "в"),



            new HandlingResponses(R.string.Hause13_his," hiz", "его"),



            new HandlingResponses(R.string.Hause14_had," h?d", "имел, получал"),



            new HandlingResponses(R.string.Hause15_do," du:", "делать"),




            new HandlingResponses(R.string.Hause16_with," wi?", "с, вместе с"),



            new HandlingResponses(R.string.Hause17_not," n?t", "не, нет; ни"),



            new HandlingResponses(R.string.Hause18_dog," d?g", "собака"),


            new HandlingResponses(R.string.Hause19_happen," h?p(?)n", "случаться"),

            new HandlingResponses(R.string.Hause20_its," its", "его, её (о предмете)"),

            new HandlingResponses(R.string.Hause21_book," buk", "книга"),

            new HandlingResponses(R.string.Hause22_morning," m?:ni?", "утро, утренний"),

            new HandlingResponses(R.string.Hause23_kid," kid", "ребёнок, малыш"),

            new HandlingResponses(R.string.Hause24_street," stri:t", "улица"),

            new HandlingResponses(R.string.Hause25_believe," bi'li:v", "верить"),



            new HandlingResponses(R.string.Hause26_talk," t?:k", "разговор; говорить"),



            new HandlingResponses(R.string.Hause27_mother," m???", "мать"),



            new HandlingResponses(R.string.Hause28_kind," kaind", "сорт, разряд; вид, класс"),



            new HandlingResponses(R.string.Hause29_coming," k?mi?", "прибытие, приезд"),



            new HandlingResponses(R.string.Hause30_each," i:t?", "каждый, всякий"),



            new HandlingResponses(R.string.Hause31_black," bl?k", "черный"),



            new HandlingResponses(R.string.Hause32_half," ha:f", "половина"),



            new HandlingResponses(R.string.Hause33_red," red", "красный, алый, рыжий"),



            new HandlingResponses(R.string.Hause34_stood," stu:d", "стоял, водружал"),


            new HandlingResponses(R.string.Hause35_young," [j??", "молодой, юный"),


            new HandlingResponses(R.string.Hause36_shall," ??l", "вспомогательный и модальный глагол"),


            new HandlingResponses(R.string.Hause37_show," ??u", "показ; показывать"),



            new HandlingResponses(R.string.Hause38_feet," fi:t", "ноги"),


            new HandlingResponses(R.string.Hause39_feel," fi:l", "чувствовать"),


            new HandlingResponses(R.string.Hause40_keep," ki:p", "держать"),



            new HandlingResponses(R.string.Hause41_next," nekst", "следующий; будущий; потом"),


            new HandlingResponses(R.string.Hause42_dark," da:k", "тёмный"),


            new HandlingResponses(R.string.Hause43_along," ?'l??", "вдоль, по, рядом"),


            new HandlingResponses(R.string.Hause44_pass," pa:s", "	проход, путь, передавать"),


            new HandlingResponses(R.string.Hause45_course," k?:s", "курс, ход, течение"),


            new HandlingResponses(R.string.Hause46_window," wind?u", "окно"),


            new HandlingResponses(R.string.Hause47_dead," ded", "мертвый"),


            new HandlingResponses(R.string.Hause48_hour," au?", "час, время"),

            new HandlingResponses(R.string.Hause49_wanted," w?ntid", "разыскиваемый полицией"),






};

    private int mCurrentIndex = 0;
    private SQLiteOpenHelper mDbHelper;

    @Override

    protected void onCreate(@Nullable Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_odin);


        mImageView1 = (ImageView)findViewById(R.id.imageView1);
        mImageView2 = (ImageView)findViewById(R.id.imageView2);
        mImageView3 = (ImageView)findViewById(R.id.imageView3) ;
        mImageView4 = (ImageView)findViewById(R.id.imageView4) ;









        textWorldView = (TextView) findViewById(R.id.textWorldView);

        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_USER_PORTRAIT);

        mRelativeLayout = (RelativeLayout) findViewById(R.id.relativeLayout);

        textTrascription = (TextView) findViewById(R.id.textTrascription);

        variant1 = (Button) findViewById(R.id.variant1);

        variant2 = (Button) findViewById(R.id.variant2);

        variant3 = (Button) findViewById(R.id.variant3);

        variant4 = (Button) findViewById(R.id.variant4);

        variant1.setOnClickListener(this);

        variant2.setOnClickListener(this);

        variant3.setOnClickListener(this);

        variant4.setOnClickListener(this);

        animTranslate = AnimationUtils.loadAnimation(AppendixOne.this, R.anim.anim_kek);
        animAlpha = AnimationUtils.loadAnimation(AppendixOne.this, R.anim.as);
        nextWord();


    }

    public void onClick(final View v) {
        ViewCompat.animate(v)
                .setDuration(200)
                .scaleX(0.9f)
                .scaleY(0.9f)
                .setInterpolator(new CycleInterpolator())
                .setListener(new ViewPropertyAnimatorListener() {
                    @Override
                    public void onAnimationStart(final View view) {

                    }

                    @Override
                    public void onAnimationEnd(View view) {
                        for (int x = 0; x <= 49; x++) {

                      if(x<=49) {
                          switch (v.getId()) {
                              case R.id.variant1:


                                  if (0 != rightAnswerRandomPosition) {
                                      variant1.setText(mQurstinBank[mCurrentIndex].getRightAnswer());
                                      variant1.setBackgroundColor(ContextCompat
                                              .getColor(AppendixOne.this, R.color.colorRed));
                                      variant1.startAnimation(animTranslate);
                                  } else {

                                      variant1.setBackgroundColor(ContextCompat
                                              .getColor(AppendixOne.this, R.color.colorGreen));
                                      nextWord();
                                      onPause();
                                      x++;

                                      mCurrentIndex = (mCurrentIndex + 1) % mQurstinBank.length;
                                      variant1.startAnimation(animAlpha);
                                      variant1.clearAnimation();
                                  }

                                  break;
                              case R.id.variant2:

                                  if (1 != rightAnswerRandomPosition) {
                                      variant2.setText(mQurstinBank[mCurrentIndex].getRightAnswer());
                                      variant2.setBackgroundColor(ContextCompat
                                              .getColor(AppendixOne.this, R.color.colorRed));
                                      mImageView2.setVisibility(View.VISIBLE);
                                      variant2.startAnimation(animTranslate);

                                  } else {

                                      variant2.setBackgroundColor(ContextCompat
                                              .getColor(AppendixOne.this, R.color.colorGreen));
                                      nextWord();
                                      onPause();
                                      x++;
                                      variant2.clearAnimation();
                                      mCurrentIndex = (mCurrentIndex + 1) % mQurstinBank.length;
                                      variant2.startAnimation(animAlpha);

                                  }

                                  break;
                              case R.id.variant3:
                                  if (2 != rightAnswerRandomPosition) {
                                      variant3.setText(mQurstinBank[mCurrentIndex].getRightAnswer());
                                      variant3.setBackgroundColor(ContextCompat
                                              .getColor(AppendixOne.this, R.color.colorRed));
                                      variant3.startAnimation(animTranslate);
                                  } else {

                                      variant3.setBackgroundColor(ContextCompat
                                              .getColor(AppendixOne.this, R.color.colorGreen));
                                      nextWord();
                                      onPause();

                                      x++;
                                      mCurrentIndex = (mCurrentIndex + 1) % mQurstinBank.length;
                                      variant1.startAnimation(animAlpha);

                                  }


                                  break;
                              case R.id.variant4:


                                  if (3 != rightAnswerRandomPosition) {
                                      variant4.setText(mQurstinBank[mCurrentIndex].getRightAnswer());
                                      variant4.setBackgroundColor(ContextCompat
                                              .getColor(AppendixOne.this, R.color.colorRed));


                                      variant4.startAnimation(animTranslate);
                                  } else {

                                      variant4.setBackgroundColor(ContextCompat
                                              .getColor(AppendixOne.this, R.color.colorGreen));
                                      nextWord();

                                      onPause();
                                      x++;

                                      mCurrentIndex = (mCurrentIndex + 1) % mQurstinBank.length;
                                      variant1.startAnimation(animAlpha);

                                  }
                                  break;
                          }
                      } else {
                          Intent intent1 = new Intent(AppendixOne.this,FinishActivity.class);
                          startActivity(intent1);

                      }
                        }
                    }

                    @Override
                    public void onAnimationCancel(final View view) {

                    }
                })
                .withLayer()
                .start();
    }



    private void nextWord() {

        textWorldView.setText(getResources().getString(mQurstinBank[mCurrentIndex].getText_id()));

        textTrascription.setText((mQurstinBank[mCurrentIndex].getTranscription()));
        answers = (List<String>) Arrays.asList(getResources().getStringArray(R.array.wrong_answers));

        Random random = new Random();
        rightAnswerRandomPosition = random.nextInt(3);

        switch (rightAnswerRandomPosition) {

            case 0:
                variant1.setText(mQurstinBank[mCurrentIndex].getRightAnswer());
                break;
            case 1:
                variant2.setText(mQurstinBank[mCurrentIndex].getRightAnswer());
                break;
            case 2:
                variant3.setText(mQurstinBank[mCurrentIndex].getRightAnswer());
                break;
            case 3:
                variant4.setText(mQurstinBank[mCurrentIndex].getRightAnswer());
                break;


        }


        variant1.setBackgroundColor(ContextCompat
                .getColor(AppendixOne.this, R.color.colorPrimary));
        variant2.setBackgroundColor(ContextCompat
                .getColor(AppendixOne.this, R.color.colorPrimary));
        variant3.setBackgroundColor(ContextCompat
                .getColor(AppendixOne.this, R.color.colorPrimary));
        variant4.setBackgroundColor(ContextCompat
                .getColor(AppendixOne.this, R.color.colorPrimary));


        position = random.nextInt(answers.size());


        if (0 != rightAnswerRandomPosition) {
            variant1.setText(answers.get(position));
        }

        position = random.nextInt(answers.size());

        if (1 != rightAnswerRandomPosition) {

            variant2.setText(answers.get(position));
        }

        position = random.nextInt(answers.size());
        if (2 != rightAnswerRandomPosition) {

            variant3.setText(answers.get(position));
        }
        position = random.nextInt(answers.size());

        if (3 != rightAnswerRandomPosition) {

            variant4.setText(answers.get(position));
        }
        mCurrentIndex = (mCurrentIndex + 1) % mQurstinBank.length;
    }

    private class CycleInterpolator implements android.view.animation.Interpolator {

        private final float mCycles = 0.5f;

        @Override
        public float getInterpolation(final float input) {
            return (float) Math.sin(2.0f * mCycles * Math.PI * input);
        }
    }


    }

