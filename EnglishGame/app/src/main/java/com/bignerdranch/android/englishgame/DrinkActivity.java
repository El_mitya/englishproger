package com.bignerdranch.android.englishgame;

import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

/**
 * Created by dmelechow on 13.08.2018.
 */
public class DrinkActivity extends AppCompatActivity {
  TextView nameText1,desriptin1,russia1;
  Button dalee;

    private static final String EXTRA_DRINKNO = "drinkNo";
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.starbuzz_activity);

          int drinkNo =(Integer)getIntent().getExtras().get(EXTRA_DRINKNO);

        try {
            SQLiteOpenHelper appendixOne = new SQLite (this);
            SQLiteDatabase db =appendixOne.getReadableDatabase();
            Cursor cursor = db.query("DRINK",new String[]{"NAME","DESCRIPTION","RUSSIA"},"_id = ?" ,
                    new String []{Integer.toString(drinkNo)},
                    null,null,null);

            if(cursor.moveToFirst()){
                String nameText = cursor.getString(0);
                String descriptionText = cursor.getString(1);
                String russia = cursor.getString(2);
                nameText1 = (TextView)findViewById(R.id.nameText);
                nameText1.setText(nameText);
                desriptin1.setText(descriptionText);
            }
cursor.close();
            db.close();
        } catch (SQLException e){
            Toast toast = Toast.makeText(this,"DateBase Invalide ",Toast.LENGTH_SHORT);
            toast.show();
        }



    }
}
